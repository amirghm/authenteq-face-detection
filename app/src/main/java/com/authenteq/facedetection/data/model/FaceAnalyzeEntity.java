package com.authenteq.facedetection.data.model;

import android.graphics.Rect;
import android.util.SparseArray;

import com.google.firebase.ml.vision.common.FirebaseVisionPoint;

import java.util.ArrayList;
import java.util.List;

public class FaceAnalyzeEntity {

    public static int MAX_PIVOT_X_DISTANCE = 100;
    public static int MAX_PIVOT_Y_DISTANCE = 100;
    public static float MIN_FRAME_AREA_FACTOR = 0.7F;
    public static float MAX_FRAME_AREA_FACTOR = 0.95F;
    public static float MAX_ROTATION = 12;


    private SparseArray<List<FirebaseVisionPoint>> contours = new SparseArray<>();
    private Rect rect = new Rect();
    private List<FirebaseVisionPoint> landmarks = new ArrayList<>();
    float headRotationY;
    float headRotationZ;

    public SparseArray<List<FirebaseVisionPoint>> getContours() {
        return contours;
    }

    public void setContours(SparseArray<List<FirebaseVisionPoint>> contours) {
        this.contours = contours;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public List<FirebaseVisionPoint> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(List<FirebaseVisionPoint> landmarks) {
        this.landmarks = landmarks;
    }

    public float getHeadRotationY() {
        return headRotationY;
    }

    public void setHeadRotationY(float headRotationY) {
        this.headRotationY = headRotationY;
    }

    public float getHeadRotationZ() {
        return headRotationZ;
    }

    public void setHeadRotationZ(float headRotationZ) {
        this.headRotationZ = headRotationZ;
    }
}
