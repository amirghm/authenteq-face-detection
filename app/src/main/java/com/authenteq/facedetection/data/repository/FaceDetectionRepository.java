package com.authenteq.facedetection.data.repository;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Rational;
import android.util.Size;
import android.util.SparseArray;

import com.authenteq.facedetection.annotations.CameraFaceDetectionMode;
import com.authenteq.facedetection.data.model.CameraConfigEntity;
import com.authenteq.facedetection.data.model.FaceAnalyzeEntity;
import com.authenteq.facedetection.utils.helpers.Log;
import com.authenteq.facedetection.utils.helpers.analyzers.FaceAnalyzer;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageAnalysisConfig;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.lifecycle.MutableLiveData;

/**
 * Created by Amir Hossein Ghasemi since 7/7/2019
 * Usage: This class holds all logic related to face analyzer of firebase and analyze inputs from
 * camera then send result to viewModel, and after that view model send it to view.
 */
@Singleton
public class FaceDetectionRepository {

    private CameraConfigEntity cameraConfigEntity;

    @Inject
    public FaceDetectionRepository() {
        this.cameraConfigEntity = new CameraConfigEntity();
        cameraConfigEntity.setCameraFacing(CameraX.LensFacing.FRONT);
    }

    public void setupFirebaseVisionFace(Integer cameraFaceDetectionMode) {
        FirebaseVisionFaceDetectorOptions.Builder firebaseVisionFaceDetectorOptionsBuilder =
                new FirebaseVisionFaceDetectorOptions.Builder();

        firebaseVisionFaceDetectorOptionsBuilder.setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST);
        firebaseVisionFaceDetectorOptionsBuilder.setMinFaceSize(0.4f);


        if (cameraFaceDetectionMode != null) {
            if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_SIMPLE)
                firebaseVisionFaceDetectorOptionsBuilder.setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS);
            else if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_LANDMARK)
                firebaseVisionFaceDetectorOptionsBuilder.setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS);
            else if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_CONTOUR)
                firebaseVisionFaceDetectorOptionsBuilder.setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS).enableTracking();

            cameraConfigEntity.setFirebaseVisionFaceDetectorOptions(firebaseVisionFaceDetectorOptionsBuilder.build());
        }
    }


    public void startFaceAnalyzer(Integer cameraFaceDetectionMode, MutableLiveData<FaceAnalyzeEntity> faceAnalyzeEntityLiveData, MutableLiveData<Exception> faceAnalyzeFailedLiveData) {
        cameraConfigEntity.getImageAnalysis().setAnalyzer(new FaceAnalyzer(FirebaseVision.getInstance().getVisionFaceDetector(cameraConfigEntity.getFirebaseVisionFaceDetectorOptions()),
                firebaseVisionFaces -> analyzeDetectedFace(firebaseVisionFaces, cameraFaceDetectionMode, faceAnalyzeEntityLiveData),
                e -> setFaceAnalyzeFailed(e, faceAnalyzeFailedLiveData)));
    }

    public Preview getCameraPreviewConfig(Preview.OnPreviewOutputUpdateListener onPreviewOutputUpdateListener) {
        if (cameraConfigEntity.getImagePreview() == null) {
            PreviewConfig.Builder previewConfig = new PreviewConfig.Builder();
            previewConfig
                    .setTargetAspectRatio(cameraConfigEntity.getCameraAspectRatio())
                    .setTargetResolution(cameraConfigEntity.getCameraSize())
                    .setLensFacing(cameraConfigEntity.getCameraFacing());
            // Every time the cameraTextureView is updated, recompute layout
            cameraConfigEntity.setImagePreview(new Preview(previewConfig.build()));
            cameraConfigEntity.getImagePreview().setOnPreviewOutputUpdateListener(onPreviewOutputUpdateListener);
        }
        return cameraConfigEntity.getImagePreview();
    }

    public ImageCapture getImageCaptureConfig() {
        if (cameraConfigEntity.getImageCapture() == null) {
            // Create configuration object for the image capture use case
            ImageCaptureConfig.Builder imageCaptureConfig = new ImageCaptureConfig.Builder()
                    .setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                    .setTargetAspectRatio(cameraConfigEntity.getCameraAspectRatio())
                    .setTargetResolution(cameraConfigEntity.getCameraSize())
                    .setLensFacing(cameraConfigEntity.getCameraFacing());

            // Build the image capture use case and attach button click listener
            cameraConfigEntity.setImageCapture(new ImageCapture(imageCaptureConfig.build()));
        }
        return cameraConfigEntity.getImageCapture();
    }

    public ImageAnalysis getAnalyzerConfig() {
        if (cameraConfigEntity.getImageAnalysis() == null) {

            if (cameraConfigEntity.getCameraAspectRatio() == null || cameraConfigEntity.getCameraSize() == null || cameraConfigEntity.getCameraFacing() == null)
                return null;

            // Setup image analysis pipeline that computes average pixel luminance
            ImageAnalysisConfig.Builder analyzerConfig = new ImageAnalysisConfig.Builder();

            // Use a worker thread for image analysis to prevent glitches
            HandlerThread analyzerThread = new HandlerThread("FaceAnalysis");
            analyzerThread.start();
            analyzerConfig.setCallbackHandler(new Handler(analyzerThread.getLooper()));
            analyzerConfig.setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
                    .setTargetAspectRatio(cameraConfigEntity.getCameraAspectRatio())
                    .setTargetResolution(cameraConfigEntity.getCameraSize())
                    .setLensFacing(cameraConfigEntity.getCameraFacing());


            // Build the image analysis use case and instantiate our analyzer
            cameraConfigEntity.setImageAnalysis(new ImageAnalysis(analyzerConfig.build()));
        }
        return cameraConfigEntity.getImageAnalysis();
    }


    private void analyzeDetectedFace(List<FirebaseVisionFace> firebaseVisionFaces, Integer cameraFaceDetectionMode, MutableLiveData<FaceAnalyzeEntity> faceAnalyzeEntityLiveData) {

        FaceAnalyzeEntity faceAnalyzeEntity = new FaceAnalyzeEntity();

        for (FirebaseVisionFace face : firebaseVisionFaces) {

            Rect bounds = face.getBoundingBox();
            float headRotationY = face.getHeadEulerAngleY();
            float headRotationZ = face.getHeadEulerAngleZ();

            if (cameraFaceDetectionMode != null) {
                if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_CONTOUR) {
                    faceAnalyzeEntity.setContours(getContourPoints(face));
                } else if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_LANDMARK) {
                    faceAnalyzeEntity.setLandmarks(getLandmarks(face));
                }
            }

            faceAnalyzeEntity.setRect(bounds);
            faceAnalyzeEntity.setHeadRotationY(headRotationY);
            faceAnalyzeEntity.setHeadRotationZ(headRotationZ);
        }
        if (firebaseVisionFaces.isEmpty()) {
            faceAnalyzeEntity = null;
        }

        faceAnalyzeEntityLiveData.setValue(faceAnalyzeEntity);
    }

    private List<FirebaseVisionPoint> getLandmarks(FirebaseVisionFace face) {
        List<FirebaseVisionPoint> landmarkPoints = new ArrayList<>();

        FirebaseVisionFaceLandmark leftEye = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE);
        if (leftEye != null) landmarkPoints.add(leftEye.getPosition());
        FirebaseVisionFaceLandmark rightEye = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE);
        if (rightEye != null) landmarkPoints.add(rightEye.getPosition());
        FirebaseVisionFaceLandmark leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR);
        if (leftEar != null) landmarkPoints.add(leftEar.getPosition());
        FirebaseVisionFaceLandmark rightEar = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EAR);
        if (rightEar != null) landmarkPoints.add(rightEar.getPosition());
        FirebaseVisionFaceLandmark leftCheek = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_CHEEK);
        if (leftCheek != null) landmarkPoints.add(leftCheek.getPosition());
        FirebaseVisionFaceLandmark rightCheek = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_CHEEK);
        if (rightCheek != null) landmarkPoints.add(rightCheek.getPosition());
        FirebaseVisionFaceLandmark noseBase = face.getLandmark(FirebaseVisionFaceLandmark.NOSE_BASE);
        if (noseBase != null) landmarkPoints.add(noseBase.getPosition());
        FirebaseVisionFaceLandmark mouthBottom = face.getLandmark(FirebaseVisionFaceLandmark.MOUTH_BOTTOM);
        if (mouthBottom != null) landmarkPoints.add(mouthBottom.getPosition());
        FirebaseVisionFaceLandmark mouthLeft = face.getLandmark(FirebaseVisionFaceLandmark.MOUTH_LEFT);
        if (mouthLeft != null) landmarkPoints.add(mouthLeft.getPosition());
        FirebaseVisionFaceLandmark mouthRight = face.getLandmark(FirebaseVisionFaceLandmark.MOUTH_RIGHT);
        if (mouthRight != null) landmarkPoints.add(mouthRight.getPosition());

        return landmarkPoints;
    }

    private SparseArray<List<FirebaseVisionPoint>> getContourPoints(FirebaseVisionFace face) {
        List<FirebaseVisionPoint> leftEyebrowTopContour =
                face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_TOP).getPoints();
        List<FirebaseVisionPoint> leftEyebrowBottomContour =
                face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_BOTTOM).getPoints();
        List<FirebaseVisionPoint> rightEyebrowTopContour =
                face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_TOP).getPoints();
        List<FirebaseVisionPoint> rightEyebrowBottomContour =
                face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_BOTTOM).getPoints();
        List<FirebaseVisionPoint> leftEyeContour =
                face.getContour(FirebaseVisionFaceContour.LEFT_EYE).getPoints();
        List<FirebaseVisionPoint> rightEyeContour =
                face.getContour(FirebaseVisionFaceContour.RIGHT_EYE).getPoints();
        List<FirebaseVisionPoint> upperLipTopContour =
                face.getContour(FirebaseVisionFaceContour.UPPER_LIP_TOP).getPoints();
        List<FirebaseVisionPoint> lowerLipTopContour =
                face.getContour(FirebaseVisionFaceContour.LOWER_LIP_TOP).getPoints();
        List<FirebaseVisionPoint> upperLipBottomContour =
                face.getContour(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM).getPoints();
        List<FirebaseVisionPoint> lowerLipBottomContour =
                face.getContour(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM).getPoints();
        List<FirebaseVisionPoint> noseBottomContour =
                face.getContour(FirebaseVisionFaceContour.NOSE_BOTTOM).getPoints();
        List<FirebaseVisionPoint> noseBridgeContour =
                face.getContour(FirebaseVisionFaceContour.NOSE_BRIDGE).getPoints();
        List<FirebaseVisionPoint> faceContour =
                face.getContour(FirebaseVisionFaceContour.FACE).getPoints();


        SparseArray<List<FirebaseVisionPoint>> points = new SparseArray<>();
        points.put(FirebaseVisionFaceContour.LEFT_EYEBROW_TOP, leftEyebrowTopContour);
        points.put(FirebaseVisionFaceContour.LEFT_EYEBROW_BOTTOM, leftEyebrowBottomContour);
        points.put(FirebaseVisionFaceContour.RIGHT_EYEBROW_TOP, rightEyebrowTopContour);
        points.put(FirebaseVisionFaceContour.RIGHT_EYEBROW_BOTTOM, rightEyebrowBottomContour);
        points.put(FirebaseVisionFaceContour.LEFT_EYE, leftEyeContour);
        points.put(FirebaseVisionFaceContour.RIGHT_EYE, rightEyeContour);
        points.put(FirebaseVisionFaceContour.NOSE_BOTTOM, noseBottomContour);
        points.put(FirebaseVisionFaceContour.NOSE_BRIDGE, noseBridgeContour);
        points.put(FirebaseVisionFaceContour.UPPER_LIP_TOP, upperLipTopContour);
        points.put(FirebaseVisionFaceContour.LOWER_LIP_TOP, lowerLipTopContour);
        points.put(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM, upperLipBottomContour);
        points.put(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM, lowerLipBottomContour);
        points.put(FirebaseVisionFaceContour.FACE, faceContour);

        return points;
    }

    private void setFaceAnalyzeFailed(Exception faceAnalyzeFailed, MutableLiveData<Exception> faceAnalyzeFailedLiveData) {
        faceAnalyzeFailedLiveData.setValue(faceAnalyzeFailed);
    }

    /**
     * This Method Checks if the face is in the frame or not
     * This method check three parameters in order to achieve better accuracy:
     * first check if face rectangle and frame in concentric
     * then check if face rectangle area not too small or large from the frame area
     * and at the end check the rotation of the face to make sure the face is completely full visible
     */
    public void detectFaceInFrame(Rect bounds, Point facePivot, Point framePivot, Point faceDetectionViewLocation, int faceFrameWidth, int faceFrameHeight, float rotationY, float rotationZ, MutableLiveData<Boolean> faceDetectedLiveData) {
        int faceArea = bounds.width() * bounds.height();
        int frameArea = faceFrameWidth * faceFrameHeight;
        framePivot.set(framePivot.x, framePivot.y - faceDetectionViewLocation.y);

        if (Math.abs(facePivot.x - framePivot.x) < FaceAnalyzeEntity.MAX_PIVOT_X_DISTANCE &&
                Math.abs(facePivot.y - framePivot.y) < FaceAnalyzeEntity.MAX_PIVOT_Y_DISTANCE &&
                faceArea > frameArea * FaceAnalyzeEntity.MIN_FRAME_AREA_FACTOR && faceArea < frameArea * FaceAnalyzeEntity.MAX_FRAME_AREA_FACTOR &&
                rotationZ > -FaceAnalyzeEntity.MAX_ROTATION && rotationZ < FaceAnalyzeEntity.MAX_ROTATION &&
                rotationY > -FaceAnalyzeEntity.MAX_ROTATION && rotationY < FaceAnalyzeEntity.MAX_ROTATION)
            faceDetectedLiveData.setValue(true);
        else
            faceDetectedLiveData.setValue(false);

        Log.d("FaceDetectionLog", "face area=" + faceArea + "  frame area=" + frameArea + " face rotationY=" + rotationY + " face rotationZ=" + rotationZ);
    }

    public void resetAnalyzerConfig() {
        cameraConfigEntity.setImageAnalysis(null);
    }


    public void setCameraSize(Size cameraSize) {
        cameraConfigEntity.setCameraSize(cameraSize);
    }

    public void setCameraAspectRatio(Rational cameraAspectRatio) {
        this.cameraConfigEntity.setCameraAspectRatio(cameraAspectRatio);
    }

    public CameraX.LensFacing getCameraFacing() {
        return cameraConfigEntity.getCameraFacing();
    }

    public ImageCapture getImageCapture() {
        return cameraConfigEntity.getImageCapture();
    }

}
