package com.authenteq.facedetection.data.model;

import android.util.Rational;
import android.util.Size;

import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;

import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;

/**
 * Created by Amir Hossein Ghasemi since 8/8/2019
 * <p>
 * Usage:
 * <p>
 * How to call:
 */
public class CameraConfigEntity {

    private ImageCapture imageCapture;
    private Preview imagePreview;
    private ImageAnalysis imageAnalysis;
    private Size cameraSize;
    private Rational cameraAspectRatio;
    private CameraX.LensFacing cameraFacing;
    private FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions;

    public ImageCapture getImageCapture() {
        return imageCapture;
    }

    public void setImageCapture(ImageCapture imageCapture) {
        this.imageCapture = imageCapture;
    }

    public Preview getImagePreview() {
        return imagePreview;
    }

    public void setImagePreview(Preview imagePreview) {
        this.imagePreview = imagePreview;
    }

    public ImageAnalysis getImageAnalysis() {
        return imageAnalysis;
    }

    public void setImageAnalysis(ImageAnalysis imageAnalysis) {
        this.imageAnalysis = imageAnalysis;
    }

    public Size getCameraSize() {
        return cameraSize;
    }

    public void setCameraSize(Size cameraSize) {
        this.cameraSize = cameraSize;
    }

    public Rational getCameraAspectRatio() {
        return cameraAspectRatio;
    }

    public void setCameraAspectRatio(Rational cameraAspectRatio) {
        this.cameraAspectRatio = cameraAspectRatio;
    }

    public CameraX.LensFacing getCameraFacing() {
        return cameraFacing;
    }

    public void setCameraFacing(CameraX.LensFacing cameraFacing) {
        this.cameraFacing = cameraFacing;
    }

    public FirebaseVisionFaceDetectorOptions getFirebaseVisionFaceDetectorOptions() {
        return firebaseVisionFaceDetectorOptions;
    }

    public void setFirebaseVisionFaceDetectorOptions(FirebaseVisionFaceDetectorOptions firebaseVisionFaceDetectorOptions) {
        this.firebaseVisionFaceDetectorOptions = firebaseVisionFaceDetectorOptions;
    }
}
