package com.authenteq.facedetection.annotations;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import kotlin.annotation.MustBeDocumented;

import static com.authenteq.facedetection.annotations.CameraFaceDetectionMode.MODE_CONTOUR;
import static com.authenteq.facedetection.annotations.CameraFaceDetectionMode.MODE_LANDMARK;
import static com.authenteq.facedetection.annotations.CameraFaceDetectionMode.MODE_SIMPLE;

@IntDef({MODE_SIMPLE, MODE_CONTOUR,MODE_LANDMARK})
@MustBeDocumented
@Retention(RetentionPolicy.SOURCE)
public @interface CameraFaceDetectionMode {
    int MODE_SIMPLE = 0;
    int MODE_CONTOUR = 1;
    int MODE_LANDMARK = 2;
}
