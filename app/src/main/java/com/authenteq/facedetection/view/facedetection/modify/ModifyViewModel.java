package com.authenteq.facedetection.view.facedetection.modify;

import android.graphics.Bitmap;
import android.graphics.Rect;

import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

public class ModifyViewModel extends ViewModel {

    private Rect imageFrameInformation;
    private String imageAddress;
    private Bitmap croppedBitmap;

    @Inject
    ModifyViewModel() { }

    public Rect getImageFrameInformation() {
        return imageFrameInformation;
    }

    public void setImageFrameInformation(Rect imageFrameInformation) {
        this.imageFrameInformation = imageFrameInformation;
    }

    public String getImageAddress() {
        return imageAddress;
    }

    public void setImageAddress(String imageAddress) {
        this.imageAddress = imageAddress;
    }

    public Bitmap getCroppedBitmap() {
        return croppedBitmap;
    }

    public void setCroppedBitmap(Bitmap croppedBitmap) {
        this.croppedBitmap = croppedBitmap;
    }
}
