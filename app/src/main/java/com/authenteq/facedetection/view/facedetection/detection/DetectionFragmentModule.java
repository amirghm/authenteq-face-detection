package com.authenteq.facedetection.view.facedetection.detection;


import com.authenteq.facedetection.data.repository.FaceDetectionRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DetectionFragmentModule {

    @Singleton
    @Provides
    FaceDetectionRepository provideFaceDetectionRepository()
    {
        return  new FaceDetectionRepository();
    }

    @Inject
    @Provides
    DetectionViewModel provideFaceDetectionViewModel(FaceDetectionRepository faceDetectionRepository){
        return new DetectionViewModel(faceDetectionRepository);
    }
}
