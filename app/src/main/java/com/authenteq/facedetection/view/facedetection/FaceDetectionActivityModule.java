package com.authenteq.facedetection.view.facedetection;


import dagger.Module;
import dagger.Provides;


@Module
public class FaceDetectionActivityModule {

    @Provides
    FaceDetectionViewModel provideFaceDetectionViewModel(){
        return new FaceDetectionViewModel();
    }

}
