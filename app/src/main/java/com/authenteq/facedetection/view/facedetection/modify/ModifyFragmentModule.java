package com.authenteq.facedetection.view.facedetection.modify;


import dagger.Module;
import dagger.Provides;


@Module
public class ModifyFragmentModule {

    @Provides
    ModifyViewModel provideModifyViewModel(){
        return new ModifyViewModel();
    }
}
