package com.authenteq.facedetection.view.facedetection.detection;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Rational;
import android.util.Size;

import com.authenteq.facedetection.annotations.CameraFaceDetectionMode;
import com.authenteq.facedetection.data.model.FaceAnalyzeEntity;
import com.authenteq.facedetection.data.repository.FaceDetectionRepository;

import javax.inject.Inject;

import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


/*
 * Created by Amir Hossein Ghasemi since 4/7/2019
 * Usage: This class is the view model of detection fragment and set configs to camera from view to repository,
 * get user inputs, send it to repository and finally get result from repository and send it to view
 */
public class DetectionViewModel extends ViewModel {


    private FaceDetectionRepository faceDetectionRepository;

    private MutableLiveData<Integer> cameraFaceDetectionModeLiveData = new MutableLiveData<>();

    private MutableLiveData<Boolean> showContourLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> showLandmarksLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> settingStateLiveData = new MutableLiveData<>();

    private MutableLiveData<FaceAnalyzeEntity> faceAnalyzeLiveData = new MutableLiveData<>();
    private MutableLiveData<Exception> faceAnalyzeFailedLiveData = new MutableLiveData<>();

    private MutableLiveData<Boolean> isAFaceDetectedInFrameLiveData = new MutableLiveData<>();

    private MutableLiveData<Integer> frameProgressSizeLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> faceFrameSeekBarMaxLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> faceFrameSeekBarInitialValueLiveData = new MutableLiveData<>();

    @Inject
    DetectionViewModel(FaceDetectionRepository faceDetectionRepository) {
        this.faceDetectionRepository = faceDetectionRepository;
        cameraFaceDetectionModeLiveData.setValue(CameraFaceDetectionMode.MODE_SIMPLE);
        faceFrameSeekBarMaxLiveData.setValue(200);
        faceFrameSeekBarInitialValueLiveData.setValue(100);
        showContourLiveData.setValue(false);
        showLandmarksLiveData.setValue(false);
        settingStateLiveData.setValue(false);
    }

    public MutableLiveData<Integer> getCameraFaceDetectionModeLiveData() {
        return cameraFaceDetectionModeLiveData;
    }

    private void setCameraFaceDetectionMode(int cameraFaceDetectionModeLiveData) {
        this.cameraFaceDetectionModeLiveData.setValue(cameraFaceDetectionModeLiveData);
    }

    public MutableLiveData<Boolean> getShowContourLiveData() {
        return showContourLiveData;
    }

    private void setShowContour(Boolean showContour) {
        this.showContourLiveData.setValue(showContour);
    }

    public MutableLiveData<Boolean> getShowLandmarksLiveData() {
        return showLandmarksLiveData;
    }

    public MutableLiveData<Boolean> getSettingStateLiveData() {
        return settingStateLiveData;
    }

    private void setSettingState(Boolean settingState) {
        this.settingStateLiveData.setValue(settingState);
    }

    public MutableLiveData<Integer> getFrameProgressSizeLiveData() {
        return frameProgressSizeLiveData;
    }

    public void setFrameProgressSize(int progressSize) {
        frameProgressSizeLiveData.setValue(progressSize);
    }

    public MutableLiveData<Boolean> isAFaceDetectedInFrameLiveData() {
        return isAFaceDetectedInFrameLiveData;
    }

    private void setShowLandmarks(Boolean showLandmarks) {
        this.showLandmarksLiveData.setValue(showLandmarks);
    }

    public void setCameraSize(Size cameraSize) {
        faceDetectionRepository.setCameraSize(cameraSize);
    }

    public void setCameraAspectRatio(Rational cameraAspectRatio) {
        faceDetectionRepository.setCameraAspectRatio(cameraAspectRatio);
    }

    public CameraX.LensFacing getCameraFacing() {
        return faceDetectionRepository.getCameraFacing();
    }

    public MutableLiveData<Integer> getFaceFrameSeekBarMaxLiveData() {
        return faceFrameSeekBarMaxLiveData;
    }

    public MutableLiveData<Integer> getFaceFrameSeekBarInitialValueLiveData() {
        return faceFrameSeekBarInitialValueLiveData;
    }

    public ImageCapture getImageCapture() {
        return faceDetectionRepository.getImageCapture();
    }

    public void handleShowLandMarksClicked() {
        Boolean showLandmarks = getShowLandmarksLiveData().getValue();
        if (showLandmarks != null) {
            if (showLandmarks)
                setCameraFaceDetectionMode(CameraFaceDetectionMode.MODE_SIMPLE);
            else
                setCameraFaceDetectionMode(CameraFaceDetectionMode.MODE_LANDMARK);
            setShowLandmarks(!showLandmarks);
            setShowContour(false);
        }
    }

    public void handleShowContourClicked() {
        Boolean showContour = getShowContourLiveData().getValue();
        if (showContour != null) {
            if (showContour)
                setCameraFaceDetectionMode(CameraFaceDetectionMode.MODE_SIMPLE);
            else
                setCameraFaceDetectionMode(CameraFaceDetectionMode.MODE_CONTOUR);
            setShowContour(!showContour);
            setShowLandmarks(false);
        }
    }

    public void handleSettingButtonClicked() {
        Boolean settingVisibility = getSettingStateLiveData().getValue();
        if (settingVisibility != null) {
            setSettingState(!settingVisibility);
        }
    }

    public MutableLiveData<FaceAnalyzeEntity> getFaceAnalyzeLiveData() {
        return faceAnalyzeLiveData;
    }

    public MutableLiveData<Exception> getFaceAnalyzeFailedLiveData() {
        return faceAnalyzeFailedLiveData;
    }


    public void resetAnalyzerConfig() {
        faceDetectionRepository.resetAnalyzerConfig();
    }

    public void setupFirebaseVisionFace() {
        faceDetectionRepository.setupFirebaseVisionFace(cameraFaceDetectionModeLiveData.getValue());
    }

    public Preview getCameraPreviewConfig(Preview.OnPreviewOutputUpdateListener onPreviewOutputUpdateListener) {
        return faceDetectionRepository.getCameraPreviewConfig(onPreviewOutputUpdateListener);
    }

    public ImageCapture getImageCaptureConfig() {
        return faceDetectionRepository.getImageCaptureConfig();
    }

    public ImageAnalysis getAnalyzerConfig() {
        return faceDetectionRepository.getAnalyzerConfig();
    }

    public void detectFaceInFrame(Rect bounds, Point facePivot, Point framePivot, Point faceDetectionViewLocation, int width, int height, float rotationY, float rotationZ) {
        faceDetectionRepository.detectFaceInFrame(bounds,facePivot,framePivot,faceDetectionViewLocation,width,height,rotationY,rotationZ,isAFaceDetectedInFrameLiveData);
    }

    public void startFaceAnalyzing() {
        faceDetectionRepository.startFaceAnalyzer(cameraFaceDetectionModeLiveData.getValue(),faceAnalyzeLiveData,faceAnalyzeFailedLiveData);
    }
}
