package com.authenteq.facedetection.view.facedetection.modify;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.authenteq.facedetection.R;
import com.authenteq.facedetection.databinding.FragmentModifyBinding;
import com.authenteq.facedetection.utils.helpers.Utils;
import com.authenteq.facedetection.view.BaseFragment;

import java.io.IOException;

public class ModifyFragment extends BaseFragment<ModifyViewModel> {

    private FragmentModifyBinding binding;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_modify,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectViewModelClass(ModifyViewModel.class);

        readArguments();
        createCroppedBitmapFromAddress();
        showCroppedBitmap();
    }

    private void readArguments() {
        if (getArguments() != null) {
            viewModel.setImageFrameInformation(ModifyFragmentArgs.fromBundle(getArguments()).getCroppedImageInformation());
            viewModel.setImageAddress(ModifyFragmentArgs.fromBundle(getArguments()).getImageAddress());
        }
    }

    /*
     *  This method create a crop from bitmap and store it inside view model
     */
    private void createCroppedBitmapFromAddress() {
        if (getActivity() != null) {

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap capturedImage = BitmapFactory.decodeFile(viewModel.getImageAddress(), options);
                Bitmap flippedAndFixedImage = Utils.flip(Utils.modifyOrientation(capturedImage, viewModel.getImageAddress()), true, false);
                correctCropInfoIfNecessary(flippedAndFixedImage);

                viewModel.setCroppedBitmap(Bitmap.createBitmap(flippedAndFixedImage,
                        viewModel.getImageFrameInformation().left,
                        viewModel.getImageFrameInformation().top,
                        viewModel.getImageFrameInformation().width(),
                        viewModel.getImageFrameInformation().height()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * This method used to make sure we don't have any coordinates more than bitmap size
    * because we get crash if reference to a outside of bitmap size
    */
    private void correctCropInfoIfNecessary(Bitmap capturedImage) {

        if(viewModel.getImageFrameInformation().top<0)
            viewModel.getImageFrameInformation().top = 0;
        if(viewModel.getImageFrameInformation().left<0)
            viewModel.getImageFrameInformation().left = 0;
        if(viewModel.getImageFrameInformation().bottom>capturedImage.getHeight())
            viewModel.getImageFrameInformation().bottom= capturedImage.getHeight();
        if(viewModel.getImageFrameInformation().right>capturedImage.getWidth())
            viewModel.getImageFrameInformation().right = capturedImage.getWidth();

    }

    private void showCroppedBitmap() {
        binding.croppedImageView.setImageBitmap(viewModel.getCroppedBitmap());
    }

}
