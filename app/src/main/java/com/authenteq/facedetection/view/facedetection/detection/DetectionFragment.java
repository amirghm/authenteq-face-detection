package com.authenteq.facedetection.view.facedetection.detection;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Rational;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;

import com.authenteq.facedetection.BuildConfig;
import com.authenteq.facedetection.R;
import com.authenteq.facedetection.data.model.FaceAnalyzeEntity;
import com.authenteq.facedetection.databinding.FragmentDetectionBinding;
import com.authenteq.facedetection.utils.helpers.Log;
import com.authenteq.facedetection.utils.helpers.Utils;
import com.authenteq.facedetection.utils.helpers.ViewAnimationUtils;
import com.authenteq.facedetection.view.BaseFragment;

import java.io.File;

/*
 * Created by Amir Hossein Ghasemi since 4/7/2019
 * Usage: This fragment is our main fragment and Used to detect face from camera input, and if a face
 * detected by face analyzer and user tap on capture image, send image and face frame bound to another
 * fragment via navigation component.
 */
public class DetectionFragment extends BaseFragment<DetectionViewModel> {

    private static int REQUEST_CODE_PERMISSIONS = 10;
    private static String[] REQUIRED_PERMISSIONS = {Manifest.permission.CAMERA};

    private FragmentDetectionBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_detection,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectViewModelClass(DetectionViewModel.class);

        configureViews();
        configureViewListeners();
        configureViewModelObservers();
    }

    /*
     * General Method for setup view and it's children
     */
    private void configureViews() {
        setupCamera();
    }

    /*
     * General Method for user callbacks and click listeners
     */
    private void configureViewListeners() {
        binding.cameraCaptureButtonImageView.setOnClickListener(view -> handleCameraButtonClicked());
        binding.showContourImageView.setOnClickListener(view -> handleShowContourClicked());
        binding.showLandMarksImageView.setOnClickListener(view -> handleShowLandMarksClicked());
        binding.settingImageView.setOnClickListener(view -> handleSettingClicked());
        binding.faceFrameSizeSeekBar.setOnSeekBarChangeListener(getOnSeekBarChangeListener());
        binding.cameraTextureView.addOnLayoutChangeListener((view, i, i1, i2, i3, i4, i5, i6, i7) -> updateTransform());
    }

    /*
     * General method for setup viewModel observers
     */
    private void configureViewModelObservers() {
        viewModel.getCameraFaceDetectionModeLiveData().observe(getViewLifecycleOwner(), this::handleOnCameraFaceDetectionModeChanged);
        viewModel.getFaceFrameSeekBarMaxLiveData().observe(getViewLifecycleOwner(),maxValue -> binding.faceFrameSizeSeekBar.setMax(maxValue));
        viewModel.getFaceFrameSeekBarInitialValueLiveData().observe(getViewLifecycleOwner(),initialValue -> binding.faceFrameSizeSeekBar.setProgress(initialValue));
        viewModel.getShowContourLiveData().observe(getViewLifecycleOwner(), this::handleShowContourChanged);
        viewModel.getShowLandmarksLiveData().observe(getViewLifecycleOwner(), this::handleShowLandmarksChanged);
        viewModel.getSettingStateLiveData().observe(getViewLifecycleOwner(), this::handleSettingState);
        viewModel.isAFaceDetectedInFrameLiveData().observe(getViewLifecycleOwner(), this::handleIsAFaceDetectedInFrameLiveData);
        viewModel.getFrameProgressSizeLiveData().observe(getViewLifecycleOwner(), this::handleFrameProgressSize);
        viewModel.getFaceAnalyzeLiveData().observe(getViewLifecycleOwner(), this::handleAnalyzedFace);
        viewModel.getFaceAnalyzeFailedLiveData().observe(getViewLifecycleOwner(), this::handleFailureFaceAnalyze);
    }

    /*
     * Request camera permissions and if we have permission we start the camera
     */
    private void setupCamera() {
        if (isCameraPermissionGranted()) {
            binding.cameraTextureView.post(this::startCamera);
        } else {
            requestCameraPermission();
        }
    }

    private void handleCameraButtonClicked() {
        if (isCameraPermissionGranted())
            takePicture();
        else
            requestCameraPermission();
    }

    private void handleShowContourClicked() {
        viewModel.handleShowContourClicked();
    }

    private void handleShowLandMarksClicked() {
        viewModel.handleShowLandMarksClicked();
    }

    private void handleSettingClicked() {
        viewModel.handleSettingButtonClicked();
    }

    private SeekBar.OnSeekBarChangeListener getOnSeekBarChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                viewModel.setFrameProgressSize(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
    }

    /*
    *   Whenever mode changed we should reset and set again analyzer in order to build it with new config
    *   and after bind that to camera we start analyzing with fresh data
    */
    private void handleOnCameraFaceDetectionModeChanged(Integer mode) {
        binding.faceDetectionView.setCameraFaceDetectionMode(mode);

        if (getAnalyzerConfig() != null) {
            CameraX.unbind(getAnalyzerConfig());
            resetAnalyzerConfig();
            setupFirebaseVisionFaceDetector();
            CameraX.bindToLifecycle(this, getAnalyzerConfig());
            startFaceAnalyzing();
        }
    }

    private void startFaceAnalyzing() {
        viewModel.startFaceAnalyzing();
    }

    private void resetAnalyzerConfig() {
        viewModel.resetAnalyzerConfig();
    }

    private void handleShowContourChanged(Boolean showContour) {
        if (getContext() != null)
            if (showContour)
                binding.showContourImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_white));
            else
                binding.showContourImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_grey500));
    }

    private void handleShowLandmarksChanged(Boolean showLandmarks) {
        if (getContext() != null)
            if (showLandmarks)
                binding.showLandMarksImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_white));
            else
                binding.showLandMarksImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_grey500));
    }

    private void handleSettingState(Boolean settingState) {
        if (getContext() != null)
            if (settingState) {
                binding.settingImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_white));
                binding.faceFrameSizeSeekBar.setVisibility(View.VISIBLE);
                ViewAnimationUtils.showView(binding.faceFrameSizeSeekBar, ViewAnimationUtils.TRANSLATE_TO_BOTTOM);
            } else {
                binding.settingImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.md_grey500));
                ViewAnimationUtils.hideView(binding.faceFrameSizeSeekBar, ViewAnimationUtils.TRANSLATE_TO_BOTTOM);
            }
    }

    private void handleFrameProgressSize(Integer progress) {
        binding.faceFrameImageView.getLayoutParams().width = Utils.dp(200, getContext()) + Utils.dp(progress - 100, getContext());
        binding.faceFrameImageView.requestLayout();
    }

    private void handleIsAFaceDetectedInFrameLiveData(Boolean isAFaceDetectedInFrame) {
        binding.cameraCaptureButtonImageView.setEnabled(isAFaceDetectedInFrame);
    }

    private void handleAnalyzedFace(FaceAnalyzeEntity faceAnalyzeEntity) {
        if (faceAnalyzeEntity != null) {
            setFaceDetectionDataInView(faceAnalyzeEntity);
            detectFaceInFrame(faceAnalyzeEntity.getRect(), faceAnalyzeEntity.getHeadRotationY(), faceAnalyzeEntity.getHeadRotationZ());
        } else {
            binding.faceDetectionView.clearData();
            disableCameraButton();
        }
    }

    private void setFaceDetectionDataInView(FaceAnalyzeEntity faceAnalyzeEntity) {
        binding.faceDetectionView.setFaceRect(faceAnalyzeEntity.getRect());
        binding.faceDetectionView.setContourPoints(faceAnalyzeEntity.getContours());
        binding.faceDetectionView.setLandmarks(faceAnalyzeEntity.getLandmarks());
        binding.faceDetectionView.invalidate();
    }

    private void handleFailureFaceAnalyze(Exception e) {
        Toast.makeText(getContext(), getString(R.string.failed_to_analyze_faces_with_error_x, e.getMessage()), Toast.LENGTH_LONG).show();
    }

    /**
     * Check if camera permission specified in the manifest have been granted
     */
    private boolean isCameraPermissionGranted() {
        if (getContext() != null)
            return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        else
            return false;
    }

    /**
     * Turn on camera on texture view and also create configuration for camera
     */
    private void startCamera() {

        setupFirebaseVisionFaceDetector();
        setupCameraConfigurations();

        if (!CameraX.isBound(getCameraPreviewConfig())) {
            CameraX.bindToLifecycle(this, getCameraPreviewConfig(), getImageCaptureConfig(), getAnalyzerConfig());
            startFaceAnalyzing();
        }
    }

    /**
     * Request camera permission
     */
    private void requestCameraPermission() {
        if (getActivity() != null)
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
    }

    private void setupFirebaseVisionFaceDetector() {
        viewModel.setupFirebaseVisionFace();
    }


    /*
    *  This method setup camera configurations and fix aspect ratio problem, because this setting sets to ui
    *  I put it in view
    */
    private void setupCameraConfigurations() {
        if (getActivity() != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            binding.cameraTextureView.getDisplay().getRealMetrics(metrics);

            // Fix camera and face detection view aspect ratio and size
            CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
            Size optimalSize = Utils.getOptimalPreviewSize(Utils.getPreviewSupportedResolution(manager, CameraCharacteristics.LENS_FACING_FRONT), metrics.widthPixels, metrics.heightPixels);

            if (optimalSize != null) {
                float optimalAspectRatio = ((float) optimalSize.getHeight() / (float) optimalSize.getWidth());
                binding.faceDetectionView.setFacingCamera(viewModel.getCameraFacing());
                binding.cameraTextureView.getLayoutParams().height = (int) (binding.cameraTextureView.getWidth() * optimalAspectRatio);
                binding.faceDetectionView.getLayoutParams().height = (int) (binding.faceDetectionView.getWidth() * optimalAspectRatio);
                binding.faceFrameImageView.post(() -> binding.faceFrameImageView.setY(binding.faceFrameImageView.getY() + Utils.getLocationOnScreen(binding.cameraTextureView).y));

                viewModel.setCameraSize(new Size(binding.cameraTextureView.getWidth(), (int) (binding.cameraTextureView.getWidth() * optimalAspectRatio)));
                viewModel.setCameraAspectRatio(new Rational(binding.cameraTextureView.getWidth(), (int) (binding.cameraTextureView.getWidth() * optimalAspectRatio)));
            }
        }
    }

    /**
     * Setup camera preview for our camera api
     */
    private Preview getCameraPreviewConfig() {
        return viewModel.getCameraPreviewConfig(getOnPreviewOutputUpdateListener());
    }

    private Preview.OnPreviewOutputUpdateListener getOnPreviewOutputUpdateListener() {
        return output -> {
            // To update the SurfaceTexture, we have to remove it and re-add it
            ViewGroup parent = (ViewGroup) binding.cameraTextureView.getParent();
            parent.removeView(binding.cameraTextureView);
            parent.addView(binding.cameraTextureView, 0);

            binding.cameraTextureView.setSurfaceTexture(output.getSurfaceTexture());
            updateTransform();
        };
    }

    /**
     * Setup image capture config for taking photos
     */
    private ImageCapture getImageCaptureConfig() {
        return viewModel.getImageCaptureConfig();
    }

    /**
     * Setup image analyzer config real time processing
     */
    private ImageAnalysis getAnalyzerConfig() {
        return viewModel.getAnalyzerConfig();
    }

    /**
     * When this method called, it takes a photo
     */
    private void takePicture() {

        if (getActivity() != null) {
            File file = new File(getActivity().getExternalMediaDirs()[0], BuildConfig.APPLICATION_ID + ".jpg");
            viewModel.getImageCapture().takePicture(file, getOnImageSavedListener());
        }
    }

    private ImageCapture.OnImageSavedListener getOnImageSavedListener() {
        return new ImageCapture.OnImageSavedListener() {
            @Override
            public void onImageSaved(@NonNull File file) {
                handleOnImageSavedResponse(file);
            }

            @Override
            public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                handleOnImageCaptureErrorResponse(message, cause);
            }
        };
    }

    private void handleOnImageCaptureErrorResponse(String message, Throwable cause) {
        String msg = getString(R.string.photo_capture_failed, message);
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        Log.d("FaceDetectionApp", msg);
        if (cause != null)
            cause.printStackTrace();
    }

    private void handleOnImageSavedResponse(File file) {
        Log.d("FaceDetectionApp", "Photo capture succeeded: " + file.getAbsolutePath());
        if (getView() != null)
            Navigation.findNavController(getView()).navigate(DetectionFragmentDirections.actionDetectionFragmentToModifyFragment(file.getAbsolutePath(), getFaceFrameRect()));
    }


    /*
    *   This Method get rectangle of frame
    */
    private Rect getFaceFrameRect() {
        if (getContext() == null) return new Rect();
        Point frameLocation = Utils.getLocationOnScreen(binding.faceFrameImageView);
        return new Rect((frameLocation.x),
                frameLocation.y - (int) binding.cameraTextureView.getY() - Utils.dp(24, getContext()),
                ((frameLocation.x + binding.faceFrameImageView.getWidth())),
                ((frameLocation.y + binding.faceFrameImageView.getHeight() - (int) binding.cameraTextureView.getY())));
    }


    private void updateTransform() {
        Matrix matrix = new Matrix();

        // Compute the center of the view finder
        float centerX = binding.cameraTextureView.getWidth() / 2f;
        float centerY = binding.cameraTextureView.getHeight() / 2f;

        // Correct preview output to account for display rotation
        int rotationDegrees;
        switch (binding.cameraTextureView.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotationDegrees = 0;
                break;
            case Surface.ROTATION_90:
                rotationDegrees = 90;
                break;
            case Surface.ROTATION_180:
                rotationDegrees = 180;
                break;
            case Surface.ROTATION_270:
                rotationDegrees = 270;
                break;
            default:
                return;
        }

        matrix.postRotate((float) -rotationDegrees, centerX, centerY);

        // Finally, apply transformations to our TextureView
        binding.cameraTextureView.setTransform(matrix);
    }


    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (isCameraPermissionGranted()) {
                binding.cameraTextureView.post(this::startCamera);
            } else {
                Toast.makeText(getActivity(),
                        "Permissions not granted by the user.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void disableCameraButton() {
        binding.cameraCaptureButtonImageView.setEnabled(false);
    }

    /*
    * This method checks if a face fit on the frame
    */
    private void detectFaceInFrame(Rect faceBounds, float rotationY, float rotationZ) {
        Point facePivot = new Point((int) binding.faceDetectionView.translateX(faceBounds.left + faceBounds.width() / 2), faceBounds.top + faceBounds.height() / 2);
        Point framePivot = Utils.getLocationPivot(binding.faceFrameImageView);
        Point faceDetectionViewLocation = Utils.getLocationOnScreen(binding.faceDetectionView);
        viewModel.detectFaceInFrame(faceBounds, facePivot, framePivot, faceDetectionViewLocation, binding.faceFrameImageView.getWidth(), binding.faceFrameImageView.getHeight(), rotationY, rotationZ);
    }


}
