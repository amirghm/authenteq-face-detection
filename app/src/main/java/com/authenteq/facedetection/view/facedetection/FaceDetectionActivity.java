package com.authenteq.facedetection.view.facedetection;

import android.os.Bundle;

import androidx.camera.core.CameraX;

import com.authenteq.facedetection.R;
import com.authenteq.facedetection.view.BaseActivity;

import javax.inject.Inject;

public class FaceDetectionActivity extends BaseActivity<FaceDetectionViewModel> {

    @Inject
    FaceDetectionViewModel viewModel;

    @Override
    public FaceDetectionViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_detection);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CameraX.unbindAll();
    }
}
