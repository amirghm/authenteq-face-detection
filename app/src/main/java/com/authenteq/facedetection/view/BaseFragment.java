package com.authenteq.facedetection.view;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public abstract class BaseFragment<T extends ViewModel> extends DaggerFragment {

    protected T viewModel;

    @Inject
    public ViewModelProvider.Factory factory;

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected void injectViewModelClass(Class<T> viewModelClass) {
        viewModel = ViewModelProviders.of(this, factory).get(viewModelClass);
    }
}