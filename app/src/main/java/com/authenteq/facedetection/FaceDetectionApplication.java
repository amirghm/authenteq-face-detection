package com.authenteq.facedetection;

import com.authenteq.facedetection.di.component.ApplicationComponent;
import com.authenteq.facedetection.di.component.DaggerApplicationComponent;
import com.google.firebase.FirebaseApp;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class FaceDetectionApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);
        return component;
    }
}
