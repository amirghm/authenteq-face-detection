package com.authenteq.facedetection.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.authenteq.facedetection.di.utils.ViewModelFactory;
import com.authenteq.facedetection.di.utils.ViewModelKey;
import com.authenteq.facedetection.view.facedetection.detection.DetectionViewModel;
import com.authenteq.facedetection.view.facedetection.modify.ModifyViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DetectionViewModel.class)
    abstract ViewModel bindDetectionViewModel(DetectionViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ModifyViewModel.class)
    abstract ViewModel bindModifyViewModel(ModifyViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}