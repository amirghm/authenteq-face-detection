package com.authenteq.facedetection.di.component;

import android.app.Application;

import com.authenteq.facedetection.FaceDetectionApplication;
import com.authenteq.facedetection.di.modules.ActivityBindingModule;
import com.authenteq.facedetection.di.modules.ContextModule;
import com.authenteq.facedetection.di.modules.FaceDetectionAppModule;
import com.authenteq.facedetection.di.modules.FragmentBindingModule;
import com.authenteq.facedetection.di.modules.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;



@Singleton
@Component(modules = {
        ContextModule.class,
        FaceDetectionAppModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBindingModule.class,
        ViewModelModule.class,
        FragmentBindingModule.class,
        })
public interface ApplicationComponent extends AndroidInjector<FaceDetectionApplication> {

    void inject(FaceDetectionApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}
