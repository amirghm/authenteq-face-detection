package com.authenteq.facedetection.di.modules;

import android.content.Context;

import com.authenteq.facedetection.FaceDetectionApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FaceDetectionAppModule {

    @Singleton
    @Provides
    Context provideContext(FaceDetectionApplication application){
        return application;
    }

}