package com.authenteq.facedetection.di.modules;

import com.authenteq.facedetection.di.scopes.FragmentScoped;
import com.authenteq.facedetection.view.facedetection.detection.DetectionFragment;
import com.authenteq.facedetection.view.facedetection.detection.DetectionFragmentModule;
import com.authenteq.facedetection.view.facedetection.modify.ModifyFragment;
import com.authenteq.facedetection.view.facedetection.modify.ModifyFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBindingModule {


    @FragmentScoped
    @ContributesAndroidInjector(modules = DetectionFragmentModule.class)
    abstract DetectionFragment provideDetectionFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = ModifyFragmentModule.class)
    abstract ModifyFragment provideModifyFragment();

}