package com.authenteq.facedetection.di.modules;

import com.authenteq.facedetection.di.scopes.ActivityScoped;
import com.authenteq.facedetection.view.facedetection.FaceDetectionActivity;
import com.authenteq.facedetection.view.facedetection.FaceDetectionActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = FaceDetectionActivityModule.class)
    abstract FaceDetectionActivity contributeFaceDetectionActivity();

}