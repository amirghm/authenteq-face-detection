package com.authenteq.facedetection.utils.helpers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;

import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;


/**
 * Created by Amir on 6/23/2017.
 */

public class ViewAnimationUtils {

    private static final int TRANSLATE_DURATION_MILLIS = 200;
    public static final int TRANSLATE_TO_TOP = 1;
    public static final int TRANSLATE_TO_BOTTOM = 2;


    public static void hideView(View view, int translateType) {
        toggleView(view, false, true, true, translateType);
    }

    public static void hideViewWithFadeAnimation(View view, final int duration) {

        view.setAlpha(1f);
        view.animate()
                .alpha(0f)
                .setInterpolator(new BounceInterpolator())
                .translationX(1000)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.animate().setListener(null);
                        view.setVisibility(View.GONE);
                    }
                });
    }


    public static void translateToLeft(View view,final int amount, final int duration) {

        view.setAlpha(1f);
        view.setTranslationX(0);
        ViewCompat.animate(view)
                .alpha(0f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .translationX(-amount)
                .setDuration(duration)
                .setListener(new ViewPropertyAnimatorListener() {

                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onAnimationEnd(View view) {
                        view.setTranslationX(-amount);
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                });
    }

    public static void translateFromLeftToBack(View view,final int amount, final int duration) {

        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.setTranslationX(-amount);
        ViewCompat.animate(view)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .translationX(0)
                .setDuration(duration)
                .setListener(new ViewPropertyAnimatorListener() {

                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onAnimationEnd(View view) {
                        view.setAlpha(1);
                        view.setTranslationX(0);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                });
    }

    public static void translateToRight(View view,final int amount, final int duration) {

        view.setAlpha(1f);
        view.setTranslationX(0);
        ViewCompat.animate(view)
                .alpha(0f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .translationX(amount)
                .setDuration(duration)
                .setListener(new ViewPropertyAnimatorListener() {

                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onAnimationEnd(View view) {
                        view.setTranslationX(amount);
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                });
    }

    public static void translateFromRightToBack(View view,final int amount, final int duration) {

        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.setTranslationX(amount);
        ViewCompat.animate(view)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .translationX(0)
                .setDuration(duration)
                .setListener(new ViewPropertyAnimatorListener() {

                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onAnimationEnd(View view) {
                        view.setAlpha(1);
                        view.setTranslationX(0);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                });
    }

    public static void showViewWithFadeAnimation(View view, final int duration) {
        view.setAlpha(0f);
        view.setTranslationX(1000);
        view.setVisibility(View.VISIBLE);
        ViewCompat.animate(view)
                .alpha(1f)
                .setInterpolator(new BounceInterpolator())
                .translationX(0)
                .setDuration(duration)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onAnimationEnd(View view) {
                        view.setAlpha(1);
                        view.setTranslationX(0);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                });;

    }


    public static void showView(View view, int translateType) {
        toggleView(view, true, true, true, translateType);
    }

    static int animatedItemCount = 0;
    private static final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();

    private static void toggleView(final View view, final boolean visible, final boolean animate, boolean force, int translateType) {

        if (force) {
            int height = view.getHeight();
            int margin = 0;
            if (translateType == TRANSLATE_TO_TOP)
                margin = (visible ? 0 : 400) * -1;
            else if (translateType == TRANSLATE_TO_BOTTOM)
                margin = visible ? 0 : 400;
            int translationY = margin;
            if (animate) {
                // Log.d("Animate", "Animating");
                if (animatedItemCount < 1)
                    ViewCompat.animate(view).setInterpolator(mInterpolator)
                            .setDuration(TRANSLATE_DURATION_MILLIS)
                            .setListener(new ViewPropertyAnimatorListener() {

                                @Override
                                public void onAnimationStart(View view) {
                                    animatedItemCount++;
                                }

                                @Override
                                public void onAnimationEnd(View view) {
                                    animatedItemCount--;
                                }

                                @Override
                                public void onAnimationCancel(View view) {

                                }

                            })
                            .translationY(translationY);
            } else {
                view.setTranslationY( translationY);
            }

        }
    }

    private static int getMarginBottom(View view) {
        int marginBottom = 0;
        final ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }
        return marginBottom;
    }

    private static int getMarginTop(View view) {
        int marginTop = 0;
        final ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            marginTop = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        }
        return marginTop;
    }
}
