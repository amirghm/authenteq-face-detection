package com.authenteq.facedetection.utils.helpers.analyzers;

import android.media.Image;
import android.util.Log;

import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;

import java.util.List;

public class FaceAnalyzer implements ImageAnalysis.Analyzer {

    private long lastAnalyzedTimestamp = 0L;
    private long detectionTimeRatio = 1000/5;
    private FirebaseVisionFaceDetector firebaseVisionFaceDetector;
    private OnSuccessListener<List<FirebaseVisionFace>> onSuccessListener;
    private OnFailureListener onFailureListener;

    public FaceAnalyzer(FirebaseVisionFaceDetector firebaseVisionFaceDetector,
                        long detectionTimeRatio,
                        OnSuccessListener<List<FirebaseVisionFace>> onSuccessListener,
                        OnFailureListener onFailureListener) {
        this.detectionTimeRatio = detectionTimeRatio;
        this.firebaseVisionFaceDetector = firebaseVisionFaceDetector;
        this.onSuccessListener = onSuccessListener;
        this.onFailureListener = onFailureListener;
    }

    public FaceAnalyzer(FirebaseVisionFaceDetector firebaseVisionFaceDetector,
                        OnSuccessListener<List<FirebaseVisionFace>> onSuccessListener,
                        OnFailureListener onFailureListener) {
        this.firebaseVisionFaceDetector = firebaseVisionFaceDetector;
        this.onSuccessListener = onSuccessListener;
        this.onFailureListener = onFailureListener;
    }


    private int degreesToFirebaseRotation(int degrees) {
        switch (degrees) {
            case 0:
                return FirebaseVisionImageMetadata.ROTATION_0;
            case 90:
                return FirebaseVisionImageMetadata.ROTATION_90;
            case 180:
                return FirebaseVisionImageMetadata.ROTATION_180;
            case 270:
                return FirebaseVisionImageMetadata.ROTATION_270;
            default:
                throw new IllegalArgumentException(
                        "Rotation must be 0, 90, 180, or 270.");
        }
    }

    int i  = 0;
    @Override
    public void analyze(ImageProxy imageProxy, int degrees) {
        if (imageProxy == null || imageProxy.getImage() == null) {
            return;
        }

        long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp - lastAnalyzedTimestamp >=
                detectionTimeRatio) {

            Log.d("FaceAnalyse", "New Data Send "+i);
            i++;

            try
            {
                Image mediaImage = imageProxy.getImage();
                int rotation = degreesToFirebaseRotation(degrees);
                FirebaseVisionImage image =
                        FirebaseVisionImage.fromMediaImage(mediaImage, rotation);


                firebaseVisionFaceDetector.detectInImage(image)
                        .addOnSuccessListener(onSuccessListener)
                        .addOnFailureListener(onFailureListener);

                lastAnalyzedTimestamp = currentTimestamp;

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}