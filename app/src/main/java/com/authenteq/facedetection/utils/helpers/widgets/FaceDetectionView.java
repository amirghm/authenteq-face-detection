package com.authenteq.facedetection.utils.helpers.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;

import androidx.camera.core.CameraX;

import com.authenteq.facedetection.annotations.CameraFaceDetectionMode;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;

import java.util.ArrayList;
import java.util.List;

public class FaceDetectionView extends View {

    SparseArray<List<FirebaseVisionPoint>> faceContours = new SparseArray<>();
    Rect faceRect = null;
    List<FirebaseVisionPoint> faceLandmarks = new ArrayList<>();
    CameraX.LensFacing lensFacing;
    @CameraFaceDetectionMode
    int cameraFaceDetectionMode;

    Paint vertexPaint;
    Paint segmentPaint;

    public FaceDetectionView(Context context) {
        super(context);
        vertexPaint = new Paint();
        segmentPaint = new Paint();
    }

    public FaceDetectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        vertexPaint = new Paint();
        segmentPaint = new Paint();
    }

    public FaceDetectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        vertexPaint = new Paint();
        segmentPaint = new Paint();
    }

    public FaceDetectionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        vertexPaint = new Paint();
        segmentPaint = new Paint();
    }


    public void setContourPoints(SparseArray<List<FirebaseVisionPoint>> faceContours) {
        this.faceContours = faceContours;
    }

    public void clearData() {
        this.faceContours = new SparseArray<>();
        this.faceLandmarks = new ArrayList<>();
        this.faceRect = null;
        invalidate();
    }

    public void setFaceRect(Rect faceRect) {
        this.faceRect = faceRect;
    }

    public void setLandmarks(List<FirebaseVisionPoint> landmarkPoints) {
        this.faceLandmarks = landmarkPoints;
    }

    public void setFacingCamera(CameraX.LensFacing lensFacing) {
        this.lensFacing = lensFacing;
    }

    public void setCameraFaceDetectionMode(int cameraFaceDetectionMode) {
        this.cameraFaceDetectionMode = cameraFaceDetectionMode;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        vertexPaint.setStyle(Paint.Style.FILL);
        vertexPaint.setColor(Color.RED);
        segmentPaint.setStyle(Paint.Style.STROKE);
        segmentPaint.setColor(Color.BLUE);
        segmentPaint.setStrokeWidth(2);

        if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_CONTOUR) {
            for (int i = 0; i < faceContours.size(); i++) {
                for (FirebaseVisionPoint item : faceContours.valueAt(i))
                    canvas.drawCircle(translateX(item.getX()), translateY(item.getY()), 5, vertexPaint);
            }


            for (int i = 0; i < faceContours.size(); i++) {
                List<FirebaseVisionPoint> pointCollection = faceContours.valueAt(i);
                for (int j = 0; j < pointCollection.size(); j++) {
                    if (j + 1 != pointCollection.size())
                        canvas.drawLine(translateX(pointCollection.get(j).getX()), translateY(pointCollection.get(j).getY()), translateX(pointCollection.get(j + 1).getX()), translateY(pointCollection.get(j + 1).getY()), segmentPaint);
                }
            }
        }

        if (cameraFaceDetectionMode == CameraFaceDetectionMode.MODE_LANDMARK) {
            if (faceRect != null) {
                canvas.drawRect(translateX(faceRect.left), translateY(faceRect.top), translateX(faceRect.right), translateY(faceRect.bottom), segmentPaint);
            }

            for (FirebaseVisionPoint item : faceLandmarks)
                canvas.drawCircle(translateX(item.getX()), translateY(item.getY()), 3, vertexPaint);
        }
    }

    /**
     * Adjusts the x coordinate from the preview's coordinate system to the view coordinate system.
     */
    public float translateX(float x) {
        if (lensFacing == CameraX.LensFacing.FRONT) {
            return getWidth() - x;
        } else {
            return x;
        }
    }

    /**
     * Adjusts the y coordinate from the preview's coordinate system to the view coordinate system.
     */
    public float translateY(float y) {
        return y;
    }

}