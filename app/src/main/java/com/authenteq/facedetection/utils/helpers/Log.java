package com.authenteq.facedetection.utils.helpers;

import com.authenteq.facedetection.BuildConfig;

public class Log {

    public static void d(String tag,String message){
        if(BuildConfig.DEBUG)
            android.util.Log.d(tag,message);
    }
}
