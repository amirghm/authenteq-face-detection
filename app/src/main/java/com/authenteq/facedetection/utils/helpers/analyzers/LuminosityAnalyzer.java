package com.authenteq.facedetection.utils.helpers.analyzers;

import android.util.Log;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class LuminosityAnalyzer implements ImageAnalysis.Analyzer {
    private long lastAnalyzedTimestamp = 0L;

    /**
     * Helper extension function used to extract a byte array from an
     * image plane buffer
     */
    private byte[] toByteArray(ByteBuffer byteBuffer) {
        return  byteBuffer.array();
    }

    @Override
    public void analyze(ImageProxy image, int rotationDegrees) {
        long currentTimestamp = System.currentTimeMillis();
        // Calculate the average luma no more often than every second
        if (currentTimestamp - lastAnalyzedTimestamp >=
            TimeUnit.SECONDS.toMillis(1)) {
            // Since format in ImageAnalysis is YUV, image.planes[0]
            // contains the Y (luminance) plane
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            // Extract image data from callback object
            byte[] data = new byte[buffer.remaining()];
            buffer.get(data);
            // Convert the data into an array of pixel values
            int[] pixels = getIntArray(data);
            // Compute average luminance for the image
            float luma = getAverage(pixels);
            // Log the new luma value
            Log.d("CameraXApp", "Average luminosity: "+luma);
            // Update timestamp of last analyzed frame
            lastAnalyzedTimestamp = currentTimestamp;
        }
    }

    private int[] getIntArray(byte[] data) {
        int[] result = new int[data.length];

        for(int i = 0;i<data.length;i++)
        {
            result[i] = (int)data[i] & 0xFF;
        }
        return result;
    }

    private float getAverage(int[] data){
        long averageValue = 0L;
        for (int item : data) {
            averageValue += item;
        }

        return ((float)averageValue)/data.length;
    }
}